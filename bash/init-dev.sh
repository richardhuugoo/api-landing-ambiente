#! /bin/bash
 
echo -e "192.168.1.84	api.dt\n" >> /etc/hosts
echo -e "192.168.1.84	landing.dt" >> /etc/hosts
 
php artisan cache:clear
php artisan route:clear
php artisan config:clear
composer dumpautoload -o

chmod 777 -R app/
chmod 777 -R config/
chmod 777 -R database/
chmod 777 -R resources/
chmod 777 -R storage/
chmod 777 -R routes/
chmod 777 -R vendor/
chmod 777 -R public/


php-fpm